import React, { Component } from "react";
import ReactTable from "react-table";
import "react-table/react-table.css";
import "./Table.css";
class Table extends Component {
  constructor() {
    super();
    this.state = {
      image: ""
    };
    this.displayImage = this.displayImage.bind(this);
  }
  // targets preview image column
  displayImage(e) {
    var str = e.target.innerHTML;
    this.setState({
      image: str
    });
    document.getElementById("overlay").style.display = "block";
  }
  // disables overlay
  disableOverlay() {
    document.getElementById("overlay").style.display = "none";
  }

  render() {
    let items = this.props.items;
    let image = <img src={this.state.image} alt="" />;
    return (
      <div>
        <ReactTable
          data={items}
          columns={[
            {
              Header: "Title",
              accessor: "title"
            },
            {
              Header: "Author",
              accessor: "author"
            },
            {
              Header: "Tags",
              accessor: "tags"
            },
            {
              Header: "Published",
              accessor: "published"
            },
            {
              Header: "Date Taken",
              accessor: "date_taken"
            },
            {
              Header: "Description",
              accessor: "description"
            },
            {
              Header: "Link",
              accessor: "link"
            },
            {
              Header: "Preview Image",
              accessor: "media.m",
              Cell: e => (
                <button onClick={this.displayImage}> {e.value} </button>
              )
            }
          ]}
          defaultPageSize={10}
          className="-striped -highlight"
        />
        <div id="overlay" onClick={this.disableOverlay}>
          <div id="image">{image}</div>
        </div>
      </div>
    );
  }
}

export default Table;
