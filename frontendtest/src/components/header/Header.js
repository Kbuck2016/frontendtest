import React, { Component } from "react";
import "./Header.css";

class Header extends Component {
  render() {
    return (
      <nav className="header">
        <h1>Flickr Photo App</h1>
      </nav>
    );
  }
}

export default Header;
