import React, { Component } from "react";
import "./SearchBar.css";

class SearchBar extends Component {
  render() {
    return (
      <div className="search_form">
        <form onSubmit={this.props.handleSubmit}>
          <input
            type="text"
            onChange={this.props.handleInput}
            name="search"
            placeholder="Enter Search"
          />
          <br />
          <input type="submit" value="Search" />
        </form>
      </div>
    );
  }
}

export default SearchBar;
