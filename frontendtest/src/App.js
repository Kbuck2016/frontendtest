import React, { Component } from "react";
import "./App.css";
import Header from "./components/header/Header";
import SearchBar from "./components/searchbar/SearchBar";
import Table from "./components/table/Table";
import axios from "axios";

let url = "";
class App extends Component {
  constructor() {
    super();
    this.state = {
      search: "",
      items: []
    };
    this.handleInput = this.handleInput.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleInput(e) {
    this.setState({
      [e.target.name]: e.target.value
    });
  }

  handleSubmit(e) {
    e.preventDefault();
    let search = this.state.search;
    url =
      "https://api.flickr.com/services/feeds/photos_public.gne?tags=" +
      search +
      "&tagmode=all&format=json&nojsoncallback=1";
    axios
      .get(url)
      .then(response => {
        this.setState({
          items: response.data.items
        });
      })
      .catch(err => {});
  }

  render() {
    console.log(this.state.items);
    console.log(this.state.search);

    return (
      <div>
        <Header />
        <SearchBar
          handleSubmit={this.handleSubmit}
          handleInput={this.handleInput}
        />
        <br />
        <Table items={this.state.items} />
        {/* Add Overlay */}
      </div>
    );
  }
}

export default App;
