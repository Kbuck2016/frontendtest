# Slalom Front End Exercise

## Instructions

Please clone down the repository and run npm install.

## Technology Used

* react.js
* react-table
* axios

## Introduction

This exercise is meant to assess your front end capabilities and ability to learn on the fly.

You are free to use any language, framework, etc as you see fit.

You are required to commit your code to github or Bitbucket. Bitbucket would most likely be easiest as you can fork directly but do not feel limited to use it and be sure to have some semblance of a git history. We would like see at least a baseline proficiency in using git so any demonstration of your knowledge is beneficial.

## The Exercise

Use the [flickr api](https://www.flickr.com/services/api/) to create a photo viewing page. The page should satisfy the following criteria:

* Can perform a query, and then perform subsequent queries without refreshing the page.
* Display a table list of flickr image metadata pertaining to the given image objects returned by the api query.
* Can click a link to preview the image as an on-page overlay
* Can sort the list of image metadata by the given fields.

A sample API response can be found [here](https://api.flickr.com/services/feeds/photos_public.gne?tags=DC&tagmode=all&format=json)
